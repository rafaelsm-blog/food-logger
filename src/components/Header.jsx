import React from 'react'

const Header = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark mb-1">
      <a className="navbar-brand" href="/">
        Food Logger
      </a>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav" />
      </div>
    </nav>
  )
}

export default Header
