import React, { Component } from 'react'
import PropTypes from 'prop-types'

class FoodForm extends Component {
  static propTypes = {
    onFormSubmit: PropTypes.func.isRequired,
  }

  static initialState = {
    date: new Date().toISOString().split('T')[0],
    name: '',
    serving: 100,
    unit: 'g',
    carbs: 0,
    protein: 0,
    fat: 0,
    calories: 0,
  }

  state = {
    ...FoodForm.initialState,
  }

  handleChange = event => {
    const input = event.currentTarget.value || ''
    const name = event.currentTarget.name
    this.setState({
      [name]: ['name', 'date', 'unit'].includes(name)
        ? input
        : parseFloat(input),
    })
  }

  handleCalculationChange = event => {
    const input = event.currentTarget.value || 0
    const name = event.currentTarget.name
    this.setState(current => {
      current[name] = parseFloat(input)
      current.calories = (current.carbs + current.protein) * 4 + current.fat * 9

      return current
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    if (this.props.onFormSubmit instanceof Function) {
      const food = { ...this.state }
      this.props.onFormSubmit(food)
      this.setState({ ...FoodForm.initialState })
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <h3>Add Food</h3>
        <div className="form-row">
          <div className="form-group col">
            <input
              type="date"
              className="form-control"
              placeholder="Date"
              onFocus={event => event.currentTarget.select()}
              onChange={this.handleChange}
              value={this.state.date}
              name="date"
              required
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col">
            <input
              type="text"
              className="form-control"
              placeholder="Name"
              onFocus={event => event.currentTarget.select()}
              onChange={this.handleChange}
              value={this.state.name}
              name="name"
              required
            />
          </div>
          <div className="form-group col">
            <input
              type="number"
              step="0.1"
              className="form-control"
              placeholder="Serving"
              onFocus={event => event.currentTarget.select()}
              onChange={this.handleChange}
              value={this.state.serving}
              name="serving"
              min="0"
            />
          </div>
          <div className="form-group col">
            <input
              type="text"
              className="form-control"
              placeholder="unit"
              onFocus={event => event.currentTarget.select()}
              onChange={this.handleChange}
              value={this.state.unit}
              name="unit"
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col">
            <input
              type="number"
              step="0.1"
              className="form-control"
              placeholder="carbs"
              onFocus={event => event.currentTarget.select()}
              onChange={this.handleCalculationChange}
              value={this.state.carbs}
              name="carbs"
              required
              min="0"
            />
          </div>
          <div className="form-group col">
            <input
              type="number"
              step="0.1"
              className="form-control"
              placeholder="protein"
              onFocus={event => event.currentTarget.select()}
              onChange={this.handleCalculationChange}
              value={this.state.protein}
              name="protein"
              required
              min="0"
            />
          </div>
          <div className="form-group col">
            <input
              type="number"
              step="0.1"
              className="form-control"
              placeholder="fat"
              onFocus={event => event.currentTarget.select()}
              onChange={this.handleCalculationChange}
              value={this.state.fat}
              name="fat"
              required
              min="0"
            />
          </div>
          <div className="form-group col">
            <input
              type="number"
              step="0.1"
              className="form-control"
              placeholder="calories"
              onFocus={event => event.currentTarget.select()}
              onChange={this.handleChange}
              value={this.state.calories}
              name="calories"
              required
            />
          </div>
        </div>
        <button className="btn btn-primary">Submit</button>
      </form>
    )
  }
}

export default FoodForm
