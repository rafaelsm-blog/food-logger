import React from 'react'

class Food extends React.Component {
  delete = () => {
    this.props.delete(this.props._key)
  }

  render() {
    const { carbs, protein, fat, calories } = this.props.food
    const total = carbs + protein + fat
    return (
      <div className="fl-food mt-2 mb-4">
        <h5>
          {this.props.food.name}
          <small className="text-muted">
            {' – '}
            {this.props.food.serving} {this.props.food.unit}
          </small>
          <button
            type="button"
            className="close"
            aria-label="Close"
            onClick={this.delete}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </h5>
        <h6>
          Calories: <strong>{calories}</strong>
        </h6>
        <span>
          <small className="mr-4">
            <strong>Carbs:</strong> {carbs}
          </small>
          <small className="mr-4">
            <strong>Protein:</strong> {protein}
          </small>
          <small className="mr-4">
            <strong>Fat:</strong> {fat}
          </small>
        </span>
        <div className="progress" style={{ height: '10px' }}>
          <div
            className="progress-bar bg-danger"
            style={{ width: Math.round((carbs / total) * 100) + '%' }}
            title={'Carbs ' + carbs}
          />
          <div
            className="progress-bar bg-success"
            style={{ width: Math.round((protein / total) * 100) + '%' }}
            title={'Protein ' + protein}
          />
          <div
            className="progress-bar bg-warning"
            style={{ width: Math.round((fat / total) * 100) + '%' }}
            title={'Fat ' + fat}
          />
        </div>
      </div>
    )
  }
}

export default Food
