import React from 'react'
import 'bootstrap-css-only'
import Header from './Header'
import Food from './Food'
import FoodForm from './FoodForm'
import FoodService from '../services/FoodService'

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = { consumed: FoodService.consumed.getAll() }
  }

  addFood = food => {
    this.setState(current => {
      const id = FoodService.consumed.add(food)
      current.consumed[id] = food
      return current
    })
  }

  deleteFood = id => {
    this.setState(current => {
      if (FoodService.consumed.delete(id)) {
        delete current[id]
      }
      return current
    })
  }

  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="container">
          <FoodForm onFormSubmit={this.addFood} />
          {Object.keys(this.state.consumed).map(key => (
            <Food
              key={key}
              _key={key}
              food={this.state.consumed[key]}
              delete={this.deleteFood}
            />
          ))}
        </div>
      </React.Fragment>
    )
  }
}

export default App
