const Consumed = (function() {
  let _consumed = {}

  const addFood = food => {
    const id = Date.now()
    _consumed[id] = food
    persist()
    return id
  }

  const getAll = () => {
    return _consumed
  }

  const deleteFood = id => {
    if (!_consumed[id]) {
      return false
    }
    delete _consumed[id]
    persist()
    return true
  }

  const persist = () => {
    const data = {
      _consumed,
    }
    window.localStorage.setItem('foodData', JSON.stringify(data))
  }

  const refresh = () => {
    let data = JSON.parse(window.localStorage.getItem('foodData')) || {}
    _consumed = data['_consumed'] || {}
  }

  refresh()

  return {
    add: addFood,
    getAll: getAll,
    delete: deleteFood,
  }
})()

export default {
  consumed: Consumed,
}
